import pytest
from typing import Set, AnyStr

from sqlalchemy.orm import Session

from closure import database, models


T_DB_URI = 'sqlite:///closure.sqlite'


@pytest.fixture
def session() -> Session:
    database.drop_db(T_DB_URI)
    database.init_db(T_DB_URI)
    yield database.get_session(T_DB_URI)


def populate_db(session: Session):
    # Thread 1 will be a comment with a reply and a reply to the reply (1->2->3)
    a1t1c1 = models.add_comment(session, author='Bob', comment='a1t1c1')
    a1t1c2 = models.add_comment(session, author='Steve', comment='a1t1c2', parent_id=a1t1c1.id)
    a1t1c3 = models.add_comment(session, author='Kathy', comment='a1t1c3', parent_id=a1t1c2.id)

    # Thread 2 is a single comment
    a1t2c1 = models.add_comment(session, author='Jeff', comment='a1t2c1')

    # Thread 3 will be a comment with 2 replies (1->[2,3])
    a1t3c1 = models.add_comment(session, author='Aaron', comment='a1t3c1')
    a1t3c2 = models.add_comment(session, author='John', comment='a1t3c2', parent_id=a1t3c1.id)
    a1t3c3 = models.add_comment(session, author='Rose', comment='a1t3c3', parent_id=a1t3c1.id)

    # Article 2 thread 1 will be a comment with 2 replies where one of the replies also has a reply (1->[2,3->[4, 5]])
    a2t1c1 = models.add_comment(session, author='Paul', comment='a2t1c1')
    a2t1c2 = models.add_comment(session, author='Carla', comment='a2t1c2', parent_id=a2t1c1.id)
    a2t1c3 = models.add_comment(session, author='Joe', comment='a2t1c3', parent_id=a2t1c1.id)
    a2t1c4 = models.add_comment(session, author='Carla', comment='a2t1c4', parent_id=a2t1c3.id)
    a2t1c5 = models.add_comment(session, author='Matt', comment='a2t1c5', parent_id=a2t1c3.id)


def all_threads(session):
    """
    Create a dictionary showing all the threads in the session, to use for before-and-after comparisons

    :param session:
    :return:
    """

    threads = {}
    for this_comment in session.query(models.Comment).all():
        threads[this_comment] = {
            'ancestors': set(models.get_comment_ancestors(session, comment_id=this_comment.id)),
            'descendants': set(models.get_comment_descendants(session, comment_id=this_comment.id))
        }
    return threads


def test_basic_descendants(session: Session):
    populate_db(session)
    a1t1c1 = session.query(models.Comment).filter_by(comment='a1t1c1').first()
    a1t1c2 = session.query(models.Comment).filter_by(comment='a1t1c2').first()
    a1t1c3 = session.query(models.Comment).filter_by(comment='a1t1c3').first()

    descendants = models.get_comment_descendants(session, comment_id=a1t1c1.id)
    dec_set = {dec.comment for dec in descendants}
    assert dec_set == {'a1t1c1', 'a1t1c2', 'a1t1c3'}

    descendants = models.get_comment_descendants(session, comment_id=a1t1c2.id)
    dec_set = {dec.comment for dec in descendants}
    assert dec_set == {'a1t1c2', 'a1t1c3'}

    descendants = models.get_comment_descendants(session, comment_id=a1t1c3.id)
    dec_set = {dec.comment for dec in descendants}
    assert dec_set == {'a1t1c3'}


@pytest.mark.parametrize(
    "comment_text, expected_ancestors",
    [
        ('a2t1c4', {'a2t1c1', 'a2t1c3', 'a2t1c4'}),
        ('a1t3c2', {'a1t3c1', 'a1t3c2'}),
        ('a1t2c1', {'a1t2c1'})
    ],
)
def test_basic_ancestors(session: Session, comment_text: AnyStr, expected_ancestors: Set[AnyStr]):
    populate_db(session)

    comment = session.query(models.Comment).filter_by(comment=comment_text).first()
    ancestors = models.get_comment_ancestors(session, comment_id=comment.id)
    anc_set = {anc.comment for anc in ancestors}
    assert anc_set == expected_ancestors


@pytest.mark.parametrize(
    "comment_text",
    [
        'a1t1c1',
        'a1t1c2',
        'a1t1c3',
        'a1t3c1',
        'a1t3c2',
        'a2t1c1',
        'a2t1c3',
        'a2t1c4',
    ],
)
def test_delete_comment(session: Session, comment_text: AnyStr):
    populate_db(session)

    # Find this comment
    comment = session.query(models.Comment).filter_by(comment=comment_text).first()

    # Get all the threads in the database
    all_starting_threads = all_threads(session)

    # Remove the threads for this comment
    del all_starting_threads[comment]

    # Delete the target comment
    models.delete_comment(session, comment=comment_text)

    # Get all the threads in the database
    all_ending_threads = all_threads(session)

    # For each relative, check that its thread is missing the target comment
    for key in all_starting_threads:
        this_starting_threads = all_starting_threads[key]
        this_ending_threads = all_ending_threads[key]
        assert this_ending_threads['ancestors'] == this_starting_threads['ancestors'] - {comment}
        assert this_ending_threads['descendants'] == this_starting_threads['descendants'] - {comment}
