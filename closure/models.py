import datetime

from typing import AnyStr, List

from sqlalchemy import Column, DateTime, ForeignKey, Integer, String
from sqlalchemy.orm import Session

from .database import Base


class Comment(Base):
    __tablename__ = "comment"

    id = Column(Integer, primary_key=True)
    author = Column(String(64), default='anonymous')
    date = Column(DateTime, default=datetime.datetime.now)
    comment = Column(String(256), nullable=False)

    def __repr__(self):
        return f"<Comment (id: {self.id})>"

    def serialize(self):
        return {'id': self.id,
                'author': self.author,
                'date': self.date.isoformat(),
                'comment': self.comment}


class CommentClosure(Base):
    __tablename__ = 'comment_closure'

    ancestor = Column(Integer, ForeignKey('comment.id'), primary_key=True)
    descendant = Column(Integer, ForeignKey('comment.id'), primary_key=True)

    def __repr__(self):
        return f"<CommentClosure (A: {self.ancestor}, D: {self.descendant})>"


def add_comment(session: Session, *, author: AnyStr, comment: AnyStr, parent_id: int = None) -> Comment:

    # Create the new comment
    comment = Comment(author=author, comment=comment)
    session.add(comment)

    # TODO: without this commit, adding closures fails. Could this leave the closure table in an inconsistent state?
    session.commit()

    # Create a self-referencing closure, and for each ancestor, add the new comment as a descendant
    ancestors = [comment.id]
    if parent_id:
        # Select all rows that reference parent_id as a descendent
        rows = session.query(CommentClosure.ancestor).filter_by(descendant=parent_id).all()
        ancestors.extend([row[0] for row in rows])

    session.add_all([CommentClosure(ancestor=ancestor, descendant=comment.id) for ancestor in ancestors])
    session.commit()

    return comment


def delete_comment(session: Session, *, comment: AnyStr):
    # Find the comment
    comment_obj = session.query(Comment).filter_by(comment=comment).first()
    comment_id = comment_obj.id

    # Delete every related closure and the comment itself
    session.query(CommentClosure).filter_by(descendant=comment_id).delete()
    session.query(CommentClosure).filter_by(ancestor=comment_id).delete()
    session.delete(comment_obj)

    session.commit()


def get_comment_ancestors(session: Session, *, comment_id: int) -> List[Comment]:
    return session.query(Comment)\
        .join(CommentClosure, Comment.id == CommentClosure.ancestor)\
        .filter(CommentClosure.descendant == comment_id)\
        .all()


def get_comment_descendants(session: Session, *, comment_id: int) -> List[Comment]:
    return session.query(Comment)\
        .join(CommentClosure, Comment.id == CommentClosure.descendant)\
        .filter(CommentClosure.ancestor == comment_id)\
        .all()
